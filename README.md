# Responsive layout for Tarqvara.com

Userarea responsive layout implemented with Bootstrap **v4.4.1** front-end for [Tarqvara.com](http://tarqvara.com).

## Demo

- [https://tarqvara.apopheoz.ru/](https://tarqvara.apopheoz.ru/)

- [https://pharmag.apopheoz.ru/](https://pharmag.apopheoz.ru/)

## Credits

- Apotheosis webdev bureau — [on Twitter](https://twitter.com/ApopheozRu), [on Web](https://apopheoz.ru)

- Bootstrap [v4.4](https://getbootstrap.com/docs/4.4/getting-started/introduction/) framework

## Author

Please contact me via [LinkedIn](https://www.linkedin.com/in/knarkhov/) or [Twitter](https://twitter.com/CondemnedCell). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).
