function checkFilter() {
    var c = Cookies.get('_pheix_shopcat_filters');
    if (!c) {
        console.log('no cookie defined: fallback and highlight {EN} filter');
        jQuery('#filter-1').addClass('common-lang-select');
        jQuery('#filter-2').removeClass('common-lang-select');
        jQuery('#filter-3').removeClass('common-lang-select');
        return;
    }
    var f = c.split("|");
    console.log( 'filter check: ' + f[0] + ',' + f[1] + ',' + f[2] );
    if ( f[0] == 1 || ( !f[0] && !f[1] && !f[2] ) ) {
        console.log('highlight {EN} filter');
        jQuery('#filter-1').addClass('common-lang-select');
        jQuery('#filter-2').removeClass('common-lang-select');
        jQuery('#filter-3').removeClass('common-lang-select');
    }
    if ( f[1] == 1 ) {
        console.log('highlight {EE} filter');
        jQuery('#filter-1').removeClass('common-lang-select');
        jQuery('#filter-2').addClass('common-lang-select');
        jQuery('#filter-3').removeClass('common-lang-select');
    }
    if ( f[2] == 1 ) {
        console.log('highlight {RU} filter');
        jQuery('#filter-1').removeClass('common-lang-select');
        jQuery('#filter-2').removeClass('common-lang-select');
        jQuery('#filter-3').addClass('common-lang-select');
    }
}

checkFilter();
