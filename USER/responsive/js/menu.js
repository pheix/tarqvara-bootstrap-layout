function dropDownMenu(myObject,url) {
    var jobj = $(myObject);
    if (jobj) {
        if ($('.navbar-toggler').is(':visible')) {
            $('.common-right-menu').find('.common-active').each(
                function( index, element ){
                    if ($(this).get(0) != jobj.get(0)) {
                        $(this).parent().find('.common-active-item-close').remove();
                        $(this).attr('onclick', "javascript:dropDownMenu(this,'" + $(this).attr('data-url') + "');");
                        $(this).removeClass('common-active');
                        console.log(
                            Math.random() +
                            ' removed class from ' +
                            $(this).text()
                        );
                    }
                }
            );
            jobj.toggleClass('common-active');
            if (!jobj.parent().find('.common-active-item-close').length) {
                jobj.before("<a href=\"javascript:closeAllSubMenus()\" class=\"common-active-item-close\">▲</a>");
                jobj.attr('onclick', "javascript:goToUrl('" + url + "');");
                jobj.attr('data-url', url);
            }
            else {
                jobj.parent().find('.common-active-item-close').remove();
                jobj.attr('onclick', "javascript:dropDownMenu(this,'" + jobj.attr('data-url') + "');");
            }
        }
        else {
            closeAllSubMenus();
            //console.log('desktop version - skip onclick drop, redirecting to ' + url);
            goToUrl(url);
        }
    }
    //console.log(jobj.text());
}

function closeAllSubMenus() {
    $('.common-right-menu').find('.common-active').each(
        function( index, element ){
            $(this).parent().find('.common-active-item-close').remove();
            $(this).attr('onclick', "javascript:dropDownMenu(this,'" + $(this).attr('data-url') + "');");
            $(this).removeClass('common-active');
        }
    );
    //$('*').removeClass('common-active');
    //$('*').find('.common-active-item-close').remove();
}

function goToUrl(url) {
    if (url && url != null && url !== 'undefined') {
        top.location.href = url;
    }
    else {
        console.log('undefined url - redirect skipped');
    }
}
